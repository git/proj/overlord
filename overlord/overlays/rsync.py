#!/usr/bin/python
# -*- coding: utf-8 -*-
#################################################################################
# overlord RSYNC OVERLAY HANDLER
#################################################################################
# File:       rsync.py
#
#             Handles rsync overlays
#
# Copyright:
#             (c) 2005 - 2008 Gunnar Wrobel
#             Distributed under the terms of the GNU General Public License v2
#
# Author(s):
#             Gunnar Wrobel <wrobel@gentoo.org>
#
''' Rsync overlay support.'''

__version__ = "$Id: rsync.py 236 2006-09-05 20:39:37Z wrobel $"

#===============================================================================
#
# Dependencies
#
#-------------------------------------------------------------------------------

from   overlord.utils             import path
from   overlord.overlays.source   import OverlaySource, require_supported

#===============================================================================
#
# Class RsyncOverlay
#
#-------------------------------------------------------------------------------

class RsyncOverlay(OverlaySource):
    ''' Handles rsync overlays.'''

    type = 'Rsync'
    type_key = 'rsync'


    def __init__(self, parent, xml, config, _location, ignore = 0, quiet = False):

        super(RsyncOverlay, self).__init__(parent, xml, config, _location, ignore, quiet)

    def add(self, base, quiet = False):
        '''Add overlay.'''

        self.supported()

        super(RsyncOverlay, self).add(base)

        return self.sync(base)

    def sync(self, base, quiet = False):
        '''Sync overlay.'''

        self.supported()

        # rsync OPTIONS [-q] SOURCE TARGET
        args = ['-rlptDvz', '--progress', '--delete', '--delete-after', '--timeout=180',
            '--exclude=distfiles/*', '--exclude=local/*', '--exclude=packages/*']
        if quiet:
            args.append('-q')
        args.append(self.src + '/')
        args.append(path([base, self.parent.name]))

        return self.run_command(*args)

    def supported(self):
        '''Overlay type supported?'''

        return require_supported([(self.command(),  'rsync',
                                         'net-misc/rsync'),])
