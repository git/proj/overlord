#!/usr/bin/env bash

#
# Copyright (C) 2010 Sebastian Pipping <sebastian@pipping.org>
#               2011 Brian Dolbec <brian.dolbec@gmail.com>
#
# Licensed under GPLv2


ret=0
for script in  overlord/tests/*.py  ; do
	echo "# PYTHONPATH=\"${PWD}\" python \"${script}\""
	PYTHONPATH="${PWD}" python "${script}" \
		|| ret=1
done

[[ ${ret} != 0 ]] && echo '!!! FAIL'
exit ${ret}
