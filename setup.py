#!/usr/bin/env python

import sys

from distutils.core import setup

# this affects the names of all the directories we do stuff with
sys.path.insert(0, './')
from layman.version import VERSION


setup(name       = 'overlord',
    version      = VERSION,
    description  = 'Python script and libraries for retrieving gentoo overlays',
    author       = 'Brian Dolbec',
    author_email = 'brian.dolbec@gmail.com',
    url          = 'http://',
    packages     = ['overlord', 'overlord.overlays'],
    scripts      = ['bin/lord'],
    license      = 'GPL',
    )
